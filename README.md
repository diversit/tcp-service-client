# TCP Service Client #

Simple service client which sends a command on a tcp port to a host and returns the response.

If you need a client like this, you're probably also interested in mocking such a service for testing. [TCP Service Mock](https://bitbucket.org/diversit/tcp-service-mock) does exactly that.

The client:

* creates and closes the connection each time a command is send.
* is build in Scala, but can also be used from Java.

## How to use this? ##

* Clone repository
* Build and deploy in (local) repository
* Import Maven dependency in your project

```
#!xml
  <dependency>
    <groupId>eu.diversit.tcp-service</groupId>
    <artifactId>tcp-service-client_2.10</artifactId>
    <version>1.0-SNAPSHOT</version>
  </dependency>
```
* Use in code:
```
#!scala
  val serviceClient = TcpServiceClient.get(host, port)
  val reply = serviceClient.sendAndReceive(command)
```

### Command line tool ###

Once you have build the project (build.sh), you can use the command line tool to send commands using the 'run.sh':
```
#!sh
run.sh <hostname> <port> <command>
```