package eu.diversit.tcpsservice

object TcpServiceClientApp extends App {

  val host = args(0)
  val port = args(1).toInt

  //  import collection.JavaConverters._
  val command = args.toList.drop(2).mkString(" ")

  println("Sending command <%s> to host %s on port %d" format (command, host, port))

  val service = TcpServiceClient.get(host, port)
  val reply = service.sendAndReceive(command)

  println("Response: %s" format reply.mkString)
}
