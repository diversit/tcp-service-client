package eu.diversit.tcpsservice

import java.net.Socket
import java.io.{InputStreamReader, BufferedReader, DataOutputStream}
import scala.concurrent._

trait TcpServiceClient {

  /**
   * Sends the command and returns the received reply.
   * @param command
   * @return reply
   */
  def sendAndReceive(command: String): String
}

case class DefaultTcpServiceClient(hostname: String, port: Int, eol: String) extends TcpServiceClient {

  override def sendAndReceive(command: String): String = {

    println("host:%s, port:%d, command:%s" format(hostname,port,command))

    val clientSocket = new Socket(hostname, port)

    send(clientSocket, command)
    val reply = receive(clientSocket)

    clientSocket.close()

    return reply
  }

  private def send(socket: Socket, command: String): Unit = {

    val outToTarget = new DataOutputStream(socket.getOutputStream)
    outToTarget.writeBytes(command + eol)
  }

  private def receive(socket: Socket): String = {
    import concurrent.duration._

    val fResponse: Future[List[Char]] = getResponse(socket)
    val response = Await.result(fResponse, 2 seconds)

    response.mkString
  }

  def getResponse(socket: Socket): Future[List[Char]] = {
    val inFromTarget = new BufferedReader(new InputStreamReader(socket.getInputStream))
    val p = promise[List[Char]]

    import concurrent.ExecutionContext.Implicits.global
    future {
      // cannot use readLine because response does not end with '\n'
      var response = List.empty[Char]
      response = response :+ inFromTarget.read.toChar
      while(inFromTarget.ready) {
        response = response :+ inFromTarget.read.toChar
      }
      p.success(response)
    }

    p.future
  }

}

object TcpServiceClient {

  def get(hostname: String, port: Int): TcpServiceClient =
      new DefaultTcpServiceClient(hostname, port, "\r\n")

  def get(hostname: String, port: Int, eol: String): TcpServiceClient =
    new DefaultTcpServiceClient(hostname, port, eol)

}
