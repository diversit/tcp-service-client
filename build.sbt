name := "TCP Service Client"

organization := "eu.diversit.tcp-service"

version := "1.0-SNAPSHOT"

scalaVersion := "2.10.3"

seq(com.github.retronym.SbtOneJar.oneJarSettings: _*)

publishMavenStyle := true

mainClass in oneJar := Some("eu.diversit.tcpsservice.TcpServiceClientApp")

